'use strict';

const fetch = require('node-fetch');
const crypto = require('crypto');
const _ = require('lodash');
const debug = require('debug')('bitmex-rest');
const WebSocket = require('ws');

const anonymousEndpoints = {
	'/funding': true,
	'/trade/bucketed': true
};

function bitmexAuthHeaders(apiKey, apiSecret, request, body, requestMoment) {
	requestMoment = requestMoment || Math.round(Date.now() / 1000);
	const expTime = requestMoment + 30; // now + 30 seconds
	const signedText = request + expTime + body;
	const signature = crypto
		.createHmac('sha256', apiSecret)
		.update(signedText).digest('hex');

	return {
		'content-type': 'application/json',
		'Accept': 'application/json',
		'api-expires': expTime,
		'api-key': apiKey,
		'api-signature': signature
	};
}

class BitMexRest {
	constructor(config) {
		this.apiKey = config.apiKey;
		this.apiSecret = config.apiSecret;
		this.baseUrl = config.baseUrl;
	}

	exec(verb, endpoint, params) {
		let requestMoment = Math.round(Date.now() / 1000);
		let url = new URL(this.baseUrl + endpoint);
		let body;
		let errRes;

		if(verb == 'GET') {
			for(let param in params) {
				if(typeof params[param] != 'string') {
					params[param] = JSON.stringify(params[param]);
				}
			}
			url.search = new URLSearchParams(params);
			body = '';
		} else
			body = JSON.stringify(params);

		const request = verb + url.pathname + url.search;
		const headers = bitmexAuthHeaders(this.apiKey, this.apiSecret, request, body, requestMoment);

		return fetch(url.toString(), {
			method: verb,
			headers: anonymousEndpoints[endpoint] ? undefined : headers,
			body: body ? body : null
		})
			.then(res => {
				const rateLimitRemaining = parseInt(res.headers.get('x-ratelimit-remaining'));
				debug('rate limit remaining:', rateLimitRemaining);
				const requestTimeTaken = Math.round(Date.now() / 1000) - requestMoment;
				debug('request to', url.toString(), 'took', requestTimeTaken, 'seconds');
				if(res.ok)
					return res.json();
				return res.json()
					.then(json => {
						throw new Error(json.error.message);
					});
			})
	}

	get(endpoint, params) {
		return this.exec('GET', endpoint, params);
	}

	post(endpoint, params) {
		return this.exec('POST', endpoint, params);
	}
}

class bitmexWS {
	constructor(config, subscriptions, dataHandler, errorHandler) {
		this.apiKey = config.apiKey;
		this.apiSecret = config.apiSecret;
		this.wsUrl = config.wsUrl;
		this.pingInterval = config.pingInterval || 5000;
		this.reconnectTimeout = config.reconnectTimeout || 3000;
		this.subscriptions = subscriptions;

		dataHandler = dataHandler || ((data) => {});
		errorHandler = errorHandler || ((data) => {});
		this.setDataHandler(dataHandler);
		this.setErrorHandler(errorHandler);

		this.connect();
	}

	connect(_this) {
		if(!_this) _this = this;
		_this.activityCounter = 0;
		_this.doReconnect = true;
		const headers = bitmexAuthHeaders(_this.apiKey, _this.apiSecret, 'GET/realtime', '');
		_this.ws = new WebSocket(_this.wsUrl, { headers });

		if(_this.subscriptions && _this.subscriptions.length) {
			_this.ws.on('open', () => {
				_this.ws.send(JSON.stringify({
					op: 'subscribe',
					args: _this.subscriptions
				}));
			});
		}

		_this.timer = setInterval(() => {
			if(_this.activityCounter == -1) {
				_this.ws.terminate();
			}
			if(_this.activityCounter == 0) {
				_this.activityCounter = -1;
				_this.ws.ping();
				debug('no messages for ' + _this.pingInterval + ' ms, sending ping');
			} else {
				_this.activityCounter = 0;
			}
		}, _this.pingInterval);

		_this.ws.on('open', () => {
			debug('ws opened');
		});

		_this.ws.on('pong', (data) => {
			debug('got pong');
			_this.activityCounter++;
		});

		_this.ws.on('message', (data) => {
			_this.activityCounter++;
			let dataObj;
			try {
				dataObj = JSON.parse(data);
			}
			catch(e) {
				_this.errorHandler(e);
				return;
			}
			_this.dataHandler(dataObj);
		});

		_this.ws.on('error', (e) => {
			debug('error:', e);
			_this.errorHandler(e);
		});

		_this.ws.on('close', () => {
			debug('closing websocket and clearing ping timer');
			clearTimeout(_this.timer);
			if(_this.doReconnect) {
				debug('connection closed, going to reconnect in ' + _this.reconnectTimeout + ' ms');
				setTimeout(_this.connect, _this.reconnectTimeout, _this);
			}
		});
	}

	setDataHandler(dataHandler) {
		this.dataHandler = dataHandler;
	}

	setErrorHandler(errorHandler) {
		this.errorHandler = errorHandler;
	}

	disconnect() {
		this.doReconnect = false;
		this.ws.close();
	}
}

module.exports = BitMexRest;
module.exports.authHeaders = bitmexAuthHeaders;
module.exports.WS = bitmexWS;
